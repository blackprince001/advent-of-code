use std::env;
use std::fmt::Error;
use std::iter::zip;
use std::path::Path;
use std::{fs::File, io::Read};

fn main() {
  let input = read_from_puzzle_input("input.txt").expect("Failed to read input.");
  let solution = part2(input);
  dbg!(solution);
}

fn read_from_puzzle_input(filename: &str) -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join(filename);
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

#[derive(Debug)]
struct Race {
  race_time: u64,
  record_distance: u64,
}

fn part2(mut input: File) -> u32 {
  let mut contents = String::new();
  input.read_to_string(&mut contents).expect("something went wrong reading the file");

  let mut lines = contents.lines();

  let mut races: Vec<Race> = vec![];

  let race_time_info = lines.next();
  let race_distance_info = lines.last();

  let race_time: Vec<&str> = race_time_info
    .unwrap()
    .split(':')
    .collect::<Vec<&str>>()
    .get(1)
    .unwrap()
    .trim()
    .split("  ")
    .map(|f| f.trim())
    .collect::<Vec<_>>();

  let distance_time: Vec<&str> = race_distance_info
    .unwrap()
    .split(':')
    .collect::<Vec<&str>>()
    .get(1)
    .unwrap()
    .trim()
    .split("  ")
    .map(|f| f.trim())
    .collect::<Vec<_>>();

  // dbg!(distance_time, race_time);

  let mut race_time_string = String::new();
  for str in race_time {
    race_time_string.push_str(str);
  }

  let mut distance = String::new();
  for str in distance_time {
    distance.push_str(str);
  }

  dbg!(&distance, &race_time_string);
  races.push(Race {
    race_time: race_time_string.parse::<u64>().unwrap(),
    record_distance: distance.parse::<u64>().unwrap(),
  });

  let mut prod = 1;
  for race in races {
    let mut possible_wins = 0;

    for i in 1..race.race_time {
      let speed = (race.race_time - i) * i;
      if speed > race.record_distance {
        possible_wins += 1
      }
    }

    prod *= possible_wins;
  }

  prod
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let mut output = String::new();
    let _ = input.read_to_string(&mut output);
    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let count = part2(input);
    assert_eq!(count, 288);
  }
}
