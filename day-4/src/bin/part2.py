from pathlib import Path

input_file_location = Path.cwd() / "day-4" / "input.txt"

cards = open(input_file_location).read().splitlines()
total = 0
qties = [1] * len(cards)

for i, card in enumerate(cards):
    winning, numbers = [set(map(int, n.split())) for n in card.split(":")[1].split("|")]

    print(winning, numbers)
    valids = len(winning & numbers)
    total += int(2 ** (valids - 1))

    for j in range(i + 1, i + valids + 1):
        qties[j] += qties[i]

print("Part 1:", total, "Part 2:", sum(qties))
