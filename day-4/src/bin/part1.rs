use std::collections::HashMap;
use std::fmt::Error;
use std::path::Path;
use std::{env, usize};
use std::{fs::File, io::Read};

fn main() {
  let input = read_from_puzzle_input("input.txt").expect("Failed to read input.");
  let solution = part1(input);
  dbg!(solution);
}

fn read_from_puzzle_input(filename: &str) -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join(filename);
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

fn part1(mut input: File) -> u32 {
  let mut contents = String::new();
  input.read_to_string(&mut contents).expect("something went wrong reading the file");

  let lines = contents.lines();

  let mut sum = 0;
  for line in lines {
    let card_info = line.split(':').collect::<Vec<&str>>();
    // let card = card_info[0];
    let (winnning_card, card) = card_info[1].split_once('|').unwrap();

    let winning_numbers: Vec<_> =
      winnning_card.trim().split(' ').filter_map(|f| f.parse::<u32>().ok()).collect();

    // dbg!(&winning_numbers);

    let card_numbers: Vec<_> =
      card.trim().split(' ').filter_map(|f| f.parse::<u32>().ok()).collect();

    // dbg!(&card_numbers);
    let mut no_of_winning_cards = 0;
    for number in card_numbers {
      if winning_numbers.contains(&number) {
        no_of_winning_cards += 1;
      }
    }

    // dbg!(winning_numbers);
    if no_of_winning_cards != 0 {
      sum += 2_u32.pow(no_of_winning_cards - 1);
    }
  }

  sum
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let mut output = String::new();
    let _ = input.read_to_string(&mut output);
    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let count = part1(input);
    assert_eq!(count, 13);
  }
}
