use std::collections::BTreeMap;
use std::env;
use std::fmt::Error;
use std::path::Path;
use std::{fs::File, io::Read};

fn main() {
  let input = read_from_puzzle_input("input.txt").expect("Failed to read input.");
  let solution = part2(input);
  dbg!(solution);
}

fn read_from_puzzle_input(filename: &str) -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join(filename);
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Ord, PartialOrd)]
struct CardSet {
  card_id: u32,
  wins: u32,
}

impl CardSet {
  fn get_wins(&self) -> u32 {
    self.wins
  }
}

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
struct CardCopy {
  card: CardSet,
  copies: u32,
}

fn part2(mut input: File) -> u32 {
  let mut contents = String::new();
  input.read_to_string(&mut contents).expect("something went wrong reading the file");

  let lines = contents.lines();

  let mut sum = 0;

  let mut all_copies_for_scratchcards: BTreeMap<CardSet, u32> = BTreeMap::new();

  let mut wins_for_each_card: Vec<CardSet> = vec![];

  let len: usize = lines.clone().count();
  let mut dp = vec![1; len];

  for (i, line) in lines.into_iter().enumerate() {
    let card_info = line.split(':').collect::<Vec<&str>>();
    let card_id = card_info[0].split_once(' ').unwrap().1.trim().parse::<u32>().unwrap();

    let (winnning_card, card) = card_info[1].split_once('|').unwrap();

    let winning_numbers: Vec<_> =
      winnning_card.trim().split(' ').filter_map(|f| f.parse::<u32>().ok()).collect();

    let card_numbers: Vec<_> =
      card.trim().split(' ').filter_map(|f| f.parse::<u32>().ok()).collect();

    let mut no_of_winning_cards = 0;
    for number in card_numbers {
      if winning_numbers.contains(&number) {
        no_of_winning_cards += 1;
      }
    }

    // if no_of_winning_cards != 0 {
    //     sum += 2_u32.pow(no_of_winning_cards - 1);
    // }

    for j in (i + 1)..(i + (no_of_winning_cards as usize) + 1) {
      dp[j] += dp[i];
    }

    wins_for_each_card.push(CardSet { card_id, wins: no_of_winning_cards });
  }

  for card in &wins_for_each_card {
    all_copies_for_scratchcards.insert(*card, 1);
  }

  // // end at generating copies for all scratchcards
  // for (card, copies) in all_copies_for_scratchcards.clone().iter() {
  //     let wins: u32 = card.get_wins();

  //     if wins == 0 {
  //         continue;
  //     }

  //     // i dont know man this should work
  //     // i honestly dont know if im not updating the copies well
  //     // or the checks are not done properly, but im feeling too lazy
  //     // to investigate the problem associated. logic checks out
  //     // code error or syntax could be in sight unnoticed.
  //     for _ in 0..*copies {
  //         for i in 1..wins + 1 {
  //             let next_card_id = card.card_id + i;
  //             let next_card = CardSet {
  //                 card_id: next_card_id,
  //                 wins: wins_for_each_card[next_card_id as usize - 1]
  //                     .get_wins(),
  //             };

  //             if all_copies_for_scratchcards.contains_key(&next_card) {
  //                 *all_copies_for_scratchcards
  //                     .get_mut(&next_card)
  //                     .unwrap() += 1;
  //             }
  //         }
  //     }
  // }

  // // we are not adding up the scratchcards so this should be incorrect
  // for (_, copies) in all_copies_for_scratchcards.into_iter() {
  //     sum += copies;
  // }

  sum = dp.into_iter().sum();

  sum
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let mut output = String::new();
    let _ = input.read_to_string(&mut output);
    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let count = part2(input);
    assert_eq!(count, 30);
  }
}
