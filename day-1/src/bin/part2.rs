use std::fmt::Error;
use std::path::Path;
use std::{env, result};
use std::{fs::File, io::Read};

fn main() {
  let cwd = env::current_dir().expect("Cannot find current directory");

  let file_path = Path::new(&cwd).join("input.txt");

  let input = File::open(file_path).expect("Failed to open file");
  let solution = part2(input);
  dbg!(solution);
}

fn read_from_puzzle_input(filename: &str) -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join(filename);
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

fn parse_text_digits(line: &String) -> impl Iterator<Item = char> + '_ {
  // let values = [
  //     "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
  // ]
  // .iter()
  // .enumerate()
  // .flat_map(|(i, text_num)| line.match_indices(text_num).map(move |_| (i as u32 + 1)));

  let mut index = 0;
  let it = std::iter::from_fn(move || {
    let reduced_line = &line[index..];
    let result = if reduced_line.starts_with("one") {
      '1'
    } else if reduced_line.starts_with("two") {
      '2'
    } else if reduced_line.starts_with("three") {
      '3'
    } else if reduced_line.starts_with("four") {
      '4'
    } else if reduced_line.starts_with("five") {
      '5'
    } else if reduced_line.starts_with("six") {
      '6'
    } else if reduced_line.starts_with("seven") {
      '7'
    } else if reduced_line.starts_with("eight") {
      '8'
    } else if reduced_line.starts_with("nine") {
      '9'
    } else {
      reduced_line.chars().next().unwrap()
    };
    index += 1;
    Some(result)
  });
  it
}

fn part2(mut input: File) -> u32 {
  let mut contents = String::new();
  input
    .read_to_string(&mut contents)
    .expect("something went wrong reading the file");

  let lines = contents.lines();

  let mut sum = 0;

  for line in lines {
    let content = &line.to_string();

    // we chain the first number check to the second one with text
    let mut it =
      parse_text_digits(content).filter_map(|character| character.to_digit(10));
    let first = it.next().expect("should be a number");

    let digit = match it.last() {
      Some(num) => format!("{first}{num}"),
      None => format!("{first}{first}"),
    }
    .parse::<u32>()
    .expect("should be a valid number");

    sum += digit;
  }

  sum
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input =
      read_from_puzzle_input("input.txt").expect("Failed to read input");

    let mut output = String::new();
    let _ = input.read_to_string(&mut output);

    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let cwd = env::current_dir().expect("Cannot find current directory");
    let file_path = Path::new(&cwd).join("test2.txt");
    let input = File::open(file_path).expect("Failed to open file");
    let solution = part2(input);

    assert!(solution > 0);
  }
}
