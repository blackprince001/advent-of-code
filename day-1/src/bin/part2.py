from pathlib import Path

input_file_location = Path.cwd() / "day-1" / "input.txt"

with open(input_file_location) as file:
    input_data = file.readlines()

ans = 0
for line in input_data:
    digits = []
    for i, char in enumerate(line):
        if char.isdigit():
            digits.append(char)

        for digit_eq, val in enumerate(
            ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
        ):
            if line[i::].startswith(val):
                digits.append(str(digit_eq + 1))
    score = int(digits[0] + digits[-1])
    ans += score

print(ans)
