use std::env;
use std::fmt::Error;
use std::path::Path;
use std::{fs::File, io::Read};

fn main() {
  let input = read_from_puzzle_input().expect("Failed to read input.");
  let solution = part1(input);
  dbg!(solution);
}

fn read_from_puzzle_input() -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join("input.txt");
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

fn part1(mut input: File) -> u32 {
  let mut contents = String::new();
  input
    .read_to_string(&mut contents)
    .expect("something went wrong reading the file");

  let lines = contents.lines();

  // let mut buf: Vec<u32> = [].to_vec();
  let mut sum = 0;
  for line in lines {
    let mut digits = line.chars().filter_map(|c| c.to_digit(10));

    let first = &digits.next().unwrap();
    let last = &digits.last().unwrap_or(*first);
    // buf.push(*first * 10 + *last);
    sum += *first * 10 + *last;
  }

  sum
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input = read_from_puzzle_input().expect("Failed to read input");

    let mut output = String::new();
    let _ = input.read_to_string(&mut output);

    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let input = File::open("test.txt").expect("Failed to open file");
    let solution = part1(input);

    assert!(solution > 0);
  }
}
