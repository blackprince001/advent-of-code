use std::collections::HashMap;
use std::fmt::Error;
use std::path::Path;
use std::{env, usize};
use std::{fs::File, io::Read};

fn main() {
  let input = read_from_puzzle_input("input.txt").expect("Failed to read input.");
  let solution = part1(input);
  dbg!(solution);
}

fn read_from_puzzle_input(filename: &str) -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join(filename);
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

fn part1(mut input: File) -> usize {
  let mut contents = String::new();
  input.read_to_string(&mut contents).expect("something went wrong reading the file");

  let lines = contents.lines();

  let mut games: HashMap<usize, Vec<HashMap<&str, usize>>> = Default::default();

  for line in lines {
    let game_info: Vec<&str> = line.split(':').collect();
    let game_id = game_info[0].split_once(' ').unwrap().1.parse::<usize>().unwrap();
    let sections_splits: Vec<&str> = game_info[1].split(';').collect();

    let mut games_played: Vec<HashMap<&str, usize>> = vec![];
    for section in &sections_splits {
      let mut game: HashMap<&str, usize> = Default::default();

      for outcome_split in section.split(',') {
        let outcome: Vec<&str> = outcome_split.trim().split(' ').collect();

        let count = outcome.first();
        let color = outcome.get(1);

        game.insert(color.unwrap(), count.unwrap().parse::<usize>().unwrap());
      }
      games_played.push(game);
    }
    games.insert(game_id, games_played);
  }

  // let mut reduced_games: HashMap<usize, HashMap<&str, usize>> =
  //     Default::default();

  // for (game_id, game) in &games {
  //     let mut reduced_map: HashMap<&str, usize> = HashMap::new();
  //     for value in game.clone() {
  //         for (color, count) in value {
  //             *reduced_map.entry(color).or_insert(0) += count;
  //         }
  //     }

  //     reduced_games.insert(*game_id, reduced_map);
  // }

  // for val in reduced_games.iter() {
  //     if *val.1.get("red").unwrap() <= 12_usize
  //         && *val.1.get("green").unwrap() <= 13_usize
  //         || *val.1.get("blue").unwrap() <= 14_usize
  //     {
  //         count += val.0;
  //     }
  // }

  let map = HashMap::from([("red", 12), ("green", 13), ("blue", 14)]);
  let mut count = 0;
  let mut possible = false;

  for (game_id, game) in games {
    for round in game {
      possible =
        round.iter().all(|(color, amount)| amount <= map.get(color).expect("color not found!"));

      if !possible {
        break;
      }
    }

    if possible {
      count += game_id;
    }
  }

  count
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let mut output = String::new();
    let _ = input.read_to_string(&mut output);
    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let count = part1(input);
    assert_eq!(count, 8);
  }

  #[test]
  fn test_output_from_map() {
    let mut input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let mut contents = String::new();
    input.read_to_string(&mut contents).expect("something went wrong reading the file");

    let lines = contents.lines();

    let mut games: HashMap<usize, Vec<HashMap<&str, usize>>> = Default::default();

    for line in lines {
      let game_info: Vec<&str> = line.split(':').collect();
      let game_id = game_info[0].split_once(' ').unwrap().1.parse::<usize>().unwrap();
      let sections_splits: Vec<&str> = game_info[1].split(';').collect();

      let mut games_played: Vec<HashMap<&str, usize>> = vec![];
      for section in &sections_splits {
        let mut game: HashMap<&str, usize> = Default::default();

        for outcome_split in section.split(',') {
          let outcome: Vec<&str> = outcome_split.trim().split(' ').collect();

          let count = outcome.first();
          let color = outcome.get(1);

          game.insert(color.unwrap(), count.unwrap().parse::<usize>().unwrap());
        }
        games_played.push(game);
      }
      games.insert(game_id, games_played);
    }

    let map = HashMap::from([("red", 12), ("green", 13), ("blue", 14)]);
    let mut count = 0;
    let mut possible = false;

    for (game_id, game) in games {
      dbg!(&game_id);
      dbg!(&game);
      for round in game {
        dbg!(&round);
        possible =
          round.iter().all(|(color, amount)| amount <= map.get(color).expect("color not found!"));
        dbg!(&possible);

        if !possible {
          break;
        }
      }

      if possible {
        count += game_id;
      }

      dbg!(count);
    }
    // let mut count = 0;
    // let mut possible = false;

    // for (game_id, game) in games {
    //     for (i, section) in game.iter().enumerate() {
    //         for (color, count) in section {
    //             if (color == &"red" && count < &12)
    //                 || (color == &"green" && count < &13)
    //                 || (color == &"blue" && count < &14)
    //             {
    //                 possible = true;
    //             }
    //         }

    //         if possible && i == section.len() - 1 {
    //             count += game_id;
    //         }
    //     }
    //     possible = false;
    // }
  }
}
