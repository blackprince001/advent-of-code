use std::collections::HashMap;
use std::fmt::Error;
use std::path::Path;
use std::{env, usize};
use std::{fs::File, io::Read};

fn main() {
  let input = read_from_puzzle_input("input.txt").expect("Failed to read input.");
  let solution = part2(input);
  dbg!(solution);
}

fn read_from_puzzle_input(filename: &str) -> Result<File, Error> {
  let cwd = env::current_dir().expect("Cannot find current directory");
  let file_path = Path::new(&cwd).join(filename);
  let input = File::open(file_path).expect("Failed to open file");

  Ok(input)
}

fn part2(mut input: File) -> usize {
  let mut contents = String::new();
  input.read_to_string(&mut contents).expect("something went wrong reading the file");

  let lines = contents.lines();

  let mut games: HashMap<usize, Vec<HashMap<&str, usize>>> = Default::default();

  for line in lines {
    let game_info: Vec<&str> = line.split(':').collect();
    let game_id = game_info[0].split_once(' ').unwrap().1.parse::<usize>().unwrap();
    let sections_splits: Vec<&str> = game_info[1].split(';').collect();

    let mut games_played: Vec<HashMap<&str, usize>> = vec![];

    for section in &sections_splits {
      let mut game: HashMap<&str, usize> = Default::default();

      for outcome_split in section.split(',') {
        let outcome: Vec<&str> = outcome_split.trim().split(' ').collect();

        let count = outcome.first();
        let color = outcome.get(1);

        game.insert(color.unwrap(), count.unwrap().parse::<usize>().unwrap());
      }
      games_played.push(game);
    }
    games.insert(game_id, games_played);
  }

  let mut reduced_games: HashMap<usize, HashMap<&str, usize>> = Default::default();
  let map = HashMap::from([("red", 12), ("green", 13), ("blue", 14)]);

  for (game_id, game) in &games {
    let mut reduced_map: HashMap<&str, usize> = HashMap::new();

    let mut rmax = 0;
    let mut gmax = 0;
    let mut bmax = 0;

    for round in game.clone() {
      for (color, count) in round {
        if color == "red" {
          rmax = std::cmp::max(rmax, count);
        } else if color == "green" {
          gmax = std::cmp::max(gmax, count);
        } else if color == "blue" {
          bmax = std::cmp::max(bmax, count);
        }
      }
    }

    for color in map.clone().into_keys() {
      if color == "red" {
        reduced_map.insert(color, rmax);
      }
      if color == "green" {
        reduced_map.insert(color, gmax);
      }
      if color == "blue" {
        reduced_map.insert(color, bmax);
      }
    }

    reduced_games.insert(*game_id, reduced_map);
  }

  let mut power = 1;
  let mut power_sum = 0;

  for (_, red_games) in reduced_games.clone() {
    for (_, count) in red_games {
      power *= count;
    }
    power_sum += power;
    power = 1;
  }

  power_sum
}

#[cfg(test)]
mod tests {

  use super::*;

  #[test]
  fn test_input_parsing() {
    let mut input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let mut output = String::new();
    let _ = input.read_to_string(&mut output);
    dbg!(output);
  }

  #[test]
  fn test_solution() {
    let input = read_from_puzzle_input("test.txt").expect("Failed to read input");
    let count = part2(input);
    assert_eq!(count, 2286);
  }
}
